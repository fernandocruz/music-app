package br.com.musicapi.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.musicapi.model.Music;
import br.com.musicapi.service.MusicService;

@Component
@Path("/musics")
public class MusicController {
	
	@Autowired
	private MusicService service;
	
	@GET
	@Produces("application/json")
	public List<Music> getAll(){
		return service.getAll();
	}
	
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Music save(Music music){
		return service.save(music);
	}
	
	
	@GET
	@Path("/name")
	@Produces("application/json")
	public List<Music> getByName(@QueryParam("name") String musicsname){
		return service.findByName(musicsname);
	}
	


}
