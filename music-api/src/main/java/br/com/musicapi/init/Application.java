package br.com.musicapi.init;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


	
@SpringBootApplication
@ComponentScan("br.com.musicapi")
@EnableJpaRepositories(basePackages = {"br.com.musicapi.repository"})
@EntityScan(basePackages = {"br.com.musicapi.model"})
public class Application extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(String[] args) {
		new Application().configure(
				new SpringApplicationBuilder(Application.class)).run(args);
	}
}


